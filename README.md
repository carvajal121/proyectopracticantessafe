# Base Proyectos

Estructura base para proyectos SafeWare

## Instalación

Pasos para hacer el primer deploy del sitio

```sh
docker-compose build
docker-compose run frontend npm install
docker-compose up -d
docker-compose exec backend python manage.py migrate
docker-compose exec backend python manage.py createsuperuser
```

## Levantar Servidor

Para levantar el servidor de desarrollo basta con hacer:

```sh
docker-compose up -d
```

El proyecto queda visible en 

 backend: http://127.0.0.1:8080/admin
 frontend: http://127.0.0.1:8000
 
Para iniciar sesión se debe usar las credenciales creadas en el paso de instalación

## Comandos Útiles

Para abrir la shell de Django
```sh
docker-compose exec backend python manage.py shell 
```
Crear migraciones (cuando se realizan cambios al modelo)
```sh
docker-compose exec backend python manage.py makemigrations 
```
Aplicar migraciones (cuando se realizan cambios al modelo)
```sh
docker-compose exec backend python manage.py migrate 
```
Crear SuperUser
```sh
docker-compose exec backend python manage.py createsuperuser 
```